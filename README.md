City Screen Artwork Generative Token webpack boilerplate
================

A boilerplate to support the development of web-based generative art for the inSpace City Screen NFT project ('A Token Genture'). Based upon the following fxhash boilerplate - https://github.com/fxhash/fxhash-webpack-boilerplate.

This project uses [webpack](https://webpack.js.org/) and [webpack-dev-server](https://github.com/webpack/webpack-dev-server) to improve the development and deployment experience.

# How to use

You will need to have [nodejs](https://nodejs.org/) installed.

## Installation

> First, make sure that your node version is >= 14

Clone the repository on your machine and move to the directory
```sh
$ git clone https://git.ecdf.ed.ac.uk/design-informatics/decade/city-screen-nft/city-screen-nft-artwork-boilerplate.git your_folder && cd your_folder
```

Install the packages required for the local environment
```sh
$ npm i
```

## Start local environment

```sh
$ npm start
```

This last command will start a local http server with [live reloading](https://webpack.js.org/configuration/dev-server/#devserverlivereload) enabled so that you can iterate faster on your projects. Open [http://localhost:8080](http://localhost:8080) to see your project in the browser.

## Build

```sh
$ npm run build
```

Will bundle your js dependencies into a single minified `bundle.js` file, move your files from the `public/` to the `dist/` folder, and link the `bundle.js` with the `index.html`.

**Moreover, it will create a `dist-zipped/project.zip` file which can be directly imported on fxhash**.

# Develop your token

Once the environment is started, you can edit the `src/index.js` file to start building your artwork. The `index.html` file is located in the `public/` folder.

You can import libraries using `npm` or by adding the library file in the `public/` folder and link those using relative paths in the `index.html`.

Any file in the `public/` folder will be added to the final project. 

## The capturedColour variable
Each artwork can be customised based on an input colour, which is provided as a URL query parameter (e.g. http://localhost:8080?colour=f2300a for the red colour '#f2300a'). You can use this colour in the `index.js` file by accessing the capturedColour variable.


# Build the final code for distribution

Once you are happy with the results, you can run the following command:

```sh
$ npm run build
```

This will create a `dist-zipped/project.zip` file, which can be used in the exhibit.

# Rules to follow

> Theses rules must be followed to ensure that your token will be future-proof, and behave in the intended way:

* the zip file must be under 15 Mb
* any path to a resource must be relative (./path/to/file.ext)
* no external resources allowed, you must put all your resources in the `public/` folder (sub-folders are OK)
* no network calls allowed (but calls to get resources from within your `public/` folder)
* you must handle any viewport size (by implementing a response to the `resize` event of the `window`)