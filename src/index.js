// Use the capturedColor variable to customise the appearance.

import * as THREE from "three";
import { GLTFLoader, OrbitControls } from "three-stdlib";

let windowWidth = window.innerWidth;
let windowHeight = window.innerHeight;
const pixelRatio = window.devicePixelRatio;

// setup three scene
let scene = new THREE.Scene();
let camera = new THREE.PerspectiveCamera(60, window.innerWidth / window.innerHeight, 1, 10000);

let renderer = new THREE.WebGLRenderer({
  antialias: true,
  preserveDrawingBuffer: true // to be able to save as PNG
});
renderer.setPixelRatio(pixelRatio);
renderer.setSize(windowWidth, windowHeight);
renderer.domElement.id = "three-canvas";
document.body.appendChild(renderer.domElement); // appends three-canvas to dom

// camera controls
const controls = new OrbitControls(camera, renderer.domElement);
controls.minDistance = 2;
controls.maxDistance = 10;
controls.autoRotate = true;
controls.autoRotateSpeed = 6;
camera.position.set(0, 0.2, 2);

// resize artwork appropriately when window dimensions change
window.addEventListener('resize', onWindowResize);

setup();
draw();

function setup() {
  // load a gltf model
  let loader = new GLTFLoader().setPath('./gltf/');
  loader.load('nft.gltf', function (gltf) {
    gltf.scene.traverse((object) => {
      if (object.isMesh) {
        let mat = new THREE.MeshStandardMaterial;
        let color = new THREE.Color(capturedColour);
        mat.color = color;
        object.material = mat;  
      }
    });
    gltf.scene.scale.set( 0.6, 0.6, 0.6 );
    gltf.scene.rotation.x = Math.PI / 2;
    scene.add(gltf.scene);
  });

  const light = new THREE.AmbientLight( 0xFFFFFF ); 
  scene.add( light );
}

function draw() {
  requestAnimationFrame(draw);

  controls.update();

  renderer.render(scene, camera);
  // add captureReady to the body class list to trigger thumbnail capture
  var body = document.body;
  setTimeout(() => {  body.classList.add("captureReady");}, 1000);
}

function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize(window.innerWidth, window.innerHeight);
}
